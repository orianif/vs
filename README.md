# Vector Sampling (VS) #


### What is this repository for? ###

Vector Sampling (VS) is a python tool to generate numerical data at missing locations into multivariate datasets 
taking into account the relation among variables under form of data patterns. It operates on matricial datasets where each 
collumn represents outcomes from the same variable and lines are multivariate patterns. 
VS will fill in missing locations with data from the same collumn and a line presenting a similar data pattern. 
For example, VS can be applied to environmental multisite measure networks (rainfall, water head, geochemical, wind, etc...), 
where each station record takes place in a collumn and all measurement in a time step will form a line. 

Version 1.0 Beta, Jan 2020, Fabio Oriani, Geological Survey of Denmark and Greenland
fabio (dot) oriani (at) protonmail (dot com)

### How to cite this software ###

Oriani, F., Stisen, S., Demirel M.C., Mariethoz, G., 2020: Missing data imputation for multisite rainfall 
networks: a comparison between geostatistical interpolation and pattern-based estimation on different terrain types, 
AMS Journal of Hydrometeorology, DOI: https://doi.org/10.1175/JHM-D-19-0220.1

### How to begin ###

To begin using the software:

_ check that the dependencies are satisfied on your machine (see below);

_ download the repository content. If you prefer, you can put VS.py in a subfolder in your python path or leave it in the local folder;

_ open a terminal and access the the repository folder;

_ from the terminal run "python3 vs_test.py" to test the code, or run the test from a python3 terminal/interface;

_ open the script vs_test.py to have more information on how to use the code and a minimal example.

### Dependencies ###

_ Python 3

_ Python packages: numpy, matplotlib, warnings.

### Need help? ###

Write Fabio : fabio (dot) oriani (at) protonmail (dot com)

### License ###

Copyright (C) 2018  Fabio Oriani

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

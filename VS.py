#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Version 1.1 Beta, Jan 2020, Fabio Oriani, University of Lausanne
"""

import numpy as np
#import matplotlib.pyplot as plt
#from joblib import Parallel, delayed
import warnings

def VS_sim(Ti,Sg,Tiaux=[],Sgaux=[],dp=2,k=10):        
    """
    Vector Sampling - generate missing data from multivariate dataset.
    
    USAGE:
        out=VS.vs_sim(Ti,Sg) fill in missing-data gaps in Sg, using the data patterns
        contained in Ti. Ti and Sg can be the same dataset.
        
        out=VS.vs_sim(Ti,Sg,dp=2), control the distance power dp used to compare data
        patterns. Default is dp=2.
        
        out=VS.vs_sim(Ti,Sg,dp=2), control the number of k-nearest patterns used to 
        estimate the missing data. Default is k=10, with k=1 the algorithm samples from the best.
    
    INPUT:
        Ti = NxM 2D numpy array containing the training dataset. N is the number
        of time steps or data patterns available for M variables. Missing
        data are indicated with NaNs (np.nan).
        
        Sg = LxM 2D numpy array containg the dataset to complete. N is the number
        of time steps or data patterns available for M variables. Missing
        data are indicated with NaNs (np.nan).
    
    OUTPUT:
        sim = a copy of Sg completed with data at missing-data locations.
    
    Fabio Oriani, University of Lausanne, Dec 2018
    fabio (dot) oriani (at) protonmail (dot com)
    
    """
    
    print(
    """
    Vector Sampling  Copyright (C) 2020 Fabio Oriani, University of Lausanne
    This program comes with ABSOLUTELY NO WARRANTY under GNU General public 
    Licence v.3 or later.
    """
    )
    
    # ADD AUX DATA IF GIVEN - (NON OPERATIVE, EXPERIMENTAL)
    auxSW=0 # no aux data
    if Tiaux!=[] and Sgaux!=[]:
        auxSW=1 # aux data present
        TiSg=np.concatenate((Ti,Sg),axis=0) # total data Ti+Sg
        TiSgaux=np.concatenate((Tiaux,Sgaux),axis=0) # total aux data Tiaux + Sgaux
        TiSgaux=TiSgaux/np.nanstd(TiSgaux,axis=0)*np.nanstd(TiSg) # standardize data to Ti+Sg std
        Ti=np.concatenate((TiSgaux[:np.shape(Ti)[0],:],Ti),axis=1) # add aux to Ti
        Sg=np.concatenate((TiSgaux[np.shape(Ti)[0]:,:],Sg),axis=1) # add aux to Sg
        
    
    # SAMPLING HISTORICAL DATA
    print("SAMPLING FROM TRAINING DATA...")
    out=np.copy(Sg)
    tinind=np.isnan(Ti) # nan in the ti (noone if mode is ="interp")
    oldprog=0 # progress
    for j in range(0,2000):
        nanind=np.isnan(out) # nan in the simulation at present
        dataind=np.logical_not(nanind) # data in the simulation at present
        prog=sum(np.ravel(nanind))/len(np.ravel(out))*100 # progress
        print(prog,"% remaining")
        if oldprog==prog:
            print("SIMULATION ENDED SUCCESSFULLY WITH MISSING DATA!")
            break
        elif np.any(nanind): # SAMPLING
            with warnings.catch_warnings():
                warnings.simplefilter("ignore", category=RuntimeWarning)
                warnings.simplefilter("ignore", category=FutureWarning)
#                if par==True: #PARALLEL IMPLEMENTATION (FUTURE DEVELOPMENT)
#                    inputs=range(0,np.shape(out)[0])
#                    out2 = Parallel(n_jobs=num_jobs,batch_size=1)(delayed(ds)(Ti,out,i,tinind,dataind,nanind) for i in inputs)
#                    out=out2
#                else:
                for i in range(0,np.shape(out)[0]): # scan each SG line
                    if np.any(nanind[i,:]): # if some nan exists in the i-th line
                        dout=out[i,:] # data event (line)
                        D=abs(np.power(Ti-dout,dp))  # distance to all TI lines
                        Dmean=np.array([np.nanmean(D,axis=0)]*np.shape(D)[0]) # conditional mean error 
                        D[np.logical_and(tinind,dataind[i,:])]=Dmean[np.logical_and(tinind,dataind[i,:])] # assign where candidate datum is missing
                        D=np.nanmean(D,axis=1) # mean for all lines
                        D=D+1e-6
                        D[np.all(np.isnan(Ti[:,nanind[i,:]]),1)]=np.Inf # put distance=+Inf for lines apporting no new data                  
                        #Dsind=D.argmin() # assign the data event with minimum distance
                        Sort_k_ind=np.argsort(D)[:k] # sort pattern index and take first k
                        Dsort=np.array([np.sort(D)[:k]]*np.shape(Sg)[1]).T # sorted distance, first k patterns
                        C=Ti[Sort_k_ind,:] # candidate matrix
                        Cdata=np.logical_not(np.isnan(C)) # data in C
                        Cest=np.nansum(C/Dsort,axis=0)/np.sum(1/Dsort*Cdata,axis=0) # weighted mean estimation
                        out[i,nanind[i,:]]=Cest[nanind[i,:]]
            oldprog=prog # update progress
        else:
            print("SIMULATION ENDED SUCCESSFULLY!")
            break
    
    # OUTPUT RESULT
    if auxSW==1: # remove aux data if present
        out=out[:,np.shape(Sgaux)[1]:]
    return out
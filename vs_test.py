#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec 21 18:04:51 2018

@author: Fabio Oriani, University of Lausanne
"""

#%% TEST SCRIPT FOR VS PACKAGE ON SYNTHETHIC DATASET
import numpy as np
import VS # importing from local directory, you can add VS.py to your python path
import matplotlib.pyplot as plt

print("########## TESTING VS ON A SYNTHETHIC DATASET #######################")

# 1) create a fake multisite dataset (1000 measures x 10 stations)
print("1/4 CREATING FAKE DATASET")
data=np.array([np.random.lognormal(0,1.2,2000)]*10).transpose() # random data
data=data+np.random.rand(np.shape(data)[0],np.shape(data)[1])*6 # add correlation along lines
data[data<np.quantile(data,0.3)]=0 # truncate negative data

# 2) create missing data gaps (half fo the data missing))
print("2/4 CREATING MISSING-DATA GAPS")
delta=300 # time interval size for gaps
refshp=np.shape(data)
B=np.zeros(refshp,dtype=bool)
A=np.random.randint(0,refshp[0],refshp[1])
yind=np.empty([refshp[0],delta],dtype="int")*0
for j in range(refshp[1]):
    yind[j,:]=np.mod(np.arange(A[j],delta+A[j],dtype="int"),refshp[0])
    B[yind[j,:],j]=True
ref=np.copy(data[B])
Sg=np.copy(data)
Sg[B]=np.nan

# 3) fill in the missing data gaps using as training data the same dataset
print("3/4 GENERATING MISSING DATA USING VS")
Ti=np.copy(Sg) # training dataset is the incomplete dataset itself
sim=VS.VS_sim(Ti,Sg,k=10) # launch VS (see the function help for more details)
sim[sim<0]=0
simdata=np.copy(sim[B]) # retrieve simulated data

# 4) compare with reference data
print("4/4 SHOWING THE RESULT")
plt.figure(figsize=(20,5))
plt.subplot(1,4,1)
plt.imshow(data,aspect="auto")
plt.xlabel("stations")
plt.ylabel("time")
plt.title("original data")
plt.subplot(1,4,2)
plt.imshow(Sg,aspect="auto")
plt.xlabel("stations")
plt.ylabel("time")
plt.title("missing data")
plt.subplot(1,4,3)
plt.imshow(sim,aspect="auto")
plt.xlabel("stations")
plt.ylabel("time")
plt.title("completed dataset")
plt.subplot(1,4,4)
plt.scatter(ref,simdata)
plt.xlabel("original data")
plt.ylabel("generated data")
plt.title("original data vs generated")
plt.show()

print("########### TEST COMPLETED SUCCESSFULLY!##############################")
print(
"""Run again the script to test the software on different random datasets. 
See the commented code at the bottom of the script to use it on your data."""
)
      
############## MINIMAL WORFLOW FOR YOUR DATASET #################
# 2a) To work on your dataset, you can use np.genfromtxt
# to import your data from an ASCII file (fro more info type help(np.genfromtxt))
# the following in an example code you can uncomment and edit.

## import data
#filename='file.txt' # put here your filename with local path
#data=np.genfromtxt(filename,skip_header=n,usecols=[2,3,10,...])
#mflag=-99999 # put here the value used for missing data inside your dataset
#data[data==mflag]=np.nan # missing data indicated with nans
#Sg=np.copy(data) # simulation grid is the incplete dataset
#Ti=np.copy(Sg) # training dataset is the incomplete dataset as well (can be different)
#sim=VS.VS_sim(Ti,Sg,k=10) # launch VS (no parameters to set), sim is the dataset completed
#
##visualize data
#plt.figure(figsize=(10,5))
#plt.subplot(1,2,1)
#plt.imshow(Sg,aspect="auto")
#plt.title("missing data")
#plt.subplot(1,2,2)
#plt.imshow(sim,aspect="auto")
#plt.title("completed dataset")

